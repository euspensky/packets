# -*- coding:utf-8 -*-
import itertools
import cPickle
from abc import ABCMeta, abstractmethod
from packets.context import debug_context
import ujson

class FieldBase(object):
    """
    @ivar py_name: Название аттрибута поля в классе пакета
    @ivar name: Название поля в json
    @ivar py_default: Значение по умолчанию
    @ivar default: Значение по json
    @ivar index: Номер поля в пакете
    @ivar required: признак обязательности поля
    """
    __instances_created = 0
    index = None
    py_name = None
    name = None
    py_default = None
    default = None

    processor = None
    required = False
    override = False
    lazy = False

    def __new__(cls, *args, **kwargs):
        instance = object.__new__(cls)
        cls.__instances_created += 1
        instance.__number = cls.__instances_created
        return instance

    def __cmp__(self, other):
        return cmp(self.__number, other.__number)

    def on_packet_class_create(self, parent_field, field_name, field_index):
        """Вызывается метаклассом Packet для установки имени и номера поля"""
        assert self.py_name is None and self.index is None, 'Однин экземпляр поля используется в нескольких пакетах'
        self.py_name = field_name
        self.index = field_index
        if self.name is None:
            self.name = field_name

    def __str__(self):
        return '<%s("%s", "%s")>' % (self.__class__.__name__, self.py_name, self.name)


class PacketBase(object):
    __slots__ = []
    __fields__ = {}
    __tags__ = set()
    __raw_mapping__ = {}
    __mutable_value_fields__ = ()
    __immutable_value_fields__ = ()
    __metaclass__ = ABCMeta

    def __repr__(self):
        return '<%s>' % (', '.join(': '.join((field_name, str(getattr(self, field_name))))
                                   for field_name in self.__fields__
                                   if getattr(self, field_name) is not None))

    def __str__(self):
        return '%s%r' % (self.__class__.__name__, self)

    @classmethod
    def __subclasshook__(cls, other):
        try:
            other_mro = other.__mro__
        except AttributeError:
            # У объектов старого типа нет поля __mro__.
            pass
        else:
            if PacketBase in other_mro:
                if any((cls.__name__ == x.__name__ for x in other_mro)):
                    return True
                if cls.__name__ == other.__name__:
                    return True
        return NotImplemented

    def __init__(self, **kwargs):
        super(PacketBase, self).__init__()
        for field_name, field_processor in self.__fields__.iteritems():
            if field_name not in kwargs:
                value = field_processor.raw_to_py(None)
            else:
                value = kwargs.pop(field_name)
            setattr(self, field_name, value)
        assert not kwargs, 'Лишние аргументы: %s' % kwargs

    def update(self, namespace=None, **kwargs):
        """Обновление пакета данными сформированными внутри сервера.
        ключи - имена полей в пакете, значения - питоновские типы данных"""
        if namespace is None:
            data_iterator = kwargs.iteritems()
        else:
            assert isinstance(namespace, dict)
            data_iterator = itertools.chain(namespace.iteritems(), kwargs.iteritems())

        for field_name, py_value in data_iterator:
            if field_name not in self.__fields__:
                # Нет такого поля в этом пакете
                continue
            setattr(self, field_name, py_value)

    def update_raw(self, data=None, **kwargs):
        """Обновление пакета данными получеными извне.
        ключи - имена полей в json, значения - значения полей в json"""
        if data is None:
            data_iterator = kwargs.iteritems()
        else:
            assert isinstance(data, dict)
            data_iterator = itertools.chain(data.iteritems(), kwargs.iteritems())

        for raw_field_name, raw_value in data_iterator:
            if raw_field_name not in self.__raw_mapping__:
                continue
            field_name = self.__raw_mapping__[raw_field_name]
            field_processor = self.__fields__[field_name]
            setattr(self, field_name, field_processor.raw_to_py(raw_value))

    if __debug__:
        def __setattr__(self, key, value):
            with debug_context((self.__class__.__name__, key), '<%s>.%s'):
                if key in self.__fields__ and value is not None:
                    self.__fields__[key].check(value)
                return super(PacketBase, self).__setattr__(key, value)

    def __delattr__(self, attr):
        if attr in self.__fields__:
            setattr(self, attr, None)
        else:
            super(PacketBase, self).__delattr__(attr)

    @abstractmethod
    def _parse_raw(self, raw_data):
        """rtype: dict"""
        pass

    @classmethod
    def load(cls, raw_data):
        """загрузка пакета из данных полученных извне.
        :param raw_data: Прочитанный json
        :type raw_data: dict | list
        иначе вместо них подставятся нули соответсвущих типов
        """
        if __debug__:
            with debug_context(cls.__name__, '<%s>'):
                return cls(**cls._parse_raw(raw_data))
        else:
            return cls(**cls._parse_raw(raw_data))

    def reload(self, raw_data):
        """Аналог load, но не возвращает новый пакет, а обновляет _все_ поля в существующем"""
        parsed = self._parse_raw(raw_data)
        for field_name, field_value in self.__fields__.iteritems():
            setattr(self, field_name, parsed[field_name])

    @abstractmethod
    def dump(self):
        pass

    def __eq__(self, other):
        if isinstance(other, PacketBase):
            return self.__class__ == other.__class__ and \
                   all(getattr(self, field_name) == getattr(other, field_name) for field_name in self.__fields__)
        return False

    def __ne__(self, other):
        if isinstance(other, PacketBase):
            return self.__class__ != other.__class__ or \
                   any(getattr(self, field_name) != getattr(other, field_name) for field_name in self.__fields__)
        return True

    def dumps(self):
        return ujson.dumps(self.dump())

    def dumpz(self):
        return ujson.dumps(self.dump()).encode('zip')

    @classmethod
    def loadz(cls, s):
        return cls.load(ujson.loads(s.decode('zip')))

    @classmethod
    def loads(cls, s):
        """:rtype: cls
        """
        return cls.load(ujson.loads(s))

    def __deepcopy__(self, memo):
        # в ~3 раза быстрее обычного deepcopy
        return cPickle.loads(cPickle.dumps(self, protocol=-1))

    def clone(self):
        return self.__deepcopy__(None)

    def __iter__(self):
        for field_name in self.__fields__:
            yield field_name, getattr(self, field_name)