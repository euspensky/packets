# -*- coding:utf-8 -*-
from collections import namedtuple
from contextlib import contextmanager
import sys


def format_message(original_message, contexts):
    context_str = ''
    for value, pattern in reversed(contexts):
        context_str += pattern % value

    return '%s -> "%s"' % (context_str, original_message)


@contextmanager
def debug_context(name, pattern='.%s'):
    """Перехватывает все выкинутые внутри исключения TypeError, ValueError, AssertionError, и добавляет к их
    сообщению свой параметр"""
    try:
        yield
    except:
        context = name, pattern
        ex_type, ex_value, ex_traceback = sys.exc_info()

        if issubclass(ex_type, (TypeError, ValueError, AssertionError, KeyError)):
            if hasattr(ex_value, 'debug_contexts'):
                ex_value.debug_contexts.append(context)
            else:
                ex_value.debug_contexts = [context]
                ex_value.original_message = ex_value.args[0] if ex_value.args else ''
            ex_value.args = (format_message('%s: %s' % (ex_type.__name__, ex_value.original_message), ex_value.debug_contexts), ) + (ex_value.args[1:])

        raise ex_type, ex_value, ex_traceback
