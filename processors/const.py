# -*- coding:utf-8 -*-
from . import FieldProcessor


class Const(FieldProcessor):
    has_mutable_value = False

    def __init__(self, value):
        self._value = value

    def check_py(self, py_value):
        pass

    def check_raw(self, raw_value):
        pass

    def raw_to_py(self, raw_value):
        return self._value

    def py_to_raw(self, py_value):
        return self._value
