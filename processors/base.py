# -*- coding:utf-8 -*-
from abc import ABCMeta, abstractmethod


class FieldProcessor(object):
    __metaclass__ = ABCMeta
    has_mutable_value = True

    @abstractmethod
    def check_py(self, py_value):
        pass

    @abstractmethod
    def check_raw(self, raw_value):
        pass

    @abstractmethod
    def raw_to_py(self, raw_value):
        pass

    @abstractmethod
    def py_to_raw(self, value):
        pass


class CheckProcessor(FieldProcessor):
    def __init__(self, check):
        assert callable(check)
        self._check = check

    def check_py(self, py_value):
        if not self._check(py_value):
            raise ValueError(str(py_value))

    def check_raw(self, raw_value):
        if not self._check(raw_value):
            raise ValueError(str(raw_value))

    def raw_to_py(self, raw_value):
        return raw_value

    def py_to_raw(self, value):
        return value

