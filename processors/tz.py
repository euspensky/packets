# -*- coding:utf-8 -*-
from datetime import tzinfo
from types import StringTypes

from pytz import timezone

from . import FieldProcessor


class TimeZone(FieldProcessor):
    def check_py(self, py_value):
        assert isinstance(py_value, tzinfo)

    def check_raw(self, raw_value):
        assert isinstance(raw_value, StringTypes)

    def raw_to_py(self, raw_value):
        return timezone(raw_value)

    def py_to_raw(self, value):
        return value.zone
