# -*- coding:utf-8 -*-
from types import StringTypes

from . import FieldProcessor


class String(FieldProcessor):
    has_mutable_value = False

    def __init__(self, max_length=None, trim=False):
        self._max_length = max_length
        self._trim = trim
        assert not (trim and max_length is None)

    def check_py(self, value):
        assert isinstance(value, StringTypes), (value, type(value))
        if not self._trim and (self._max_length is not None) and len(value) > self._max_length:
            raise ValueError('Слишком длинная строка %s (можно %s)' % (len(value), self._max_length))

    check_raw = check_py

    def raw_to_py(self, raw_value):
        if self._trim:
            return raw_value[:self._max_length]
        else:
            return raw_value

    def py_to_raw(self, py_value):
        if self._trim:
            return py_value[:self._max_length]
        else:
            return py_value
