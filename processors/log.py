# -*- coding:utf-8 -*-
from enum import Enum
from types import StringTypes
import logging

from . import FieldProcessor


str_to_level = dict(
    critical=logging.CRITICAL,
    fatal=logging.FATAL,
    error=logging.ERROR,
    warning=logging.WARNING,
    warn=logging.WARNING,
    info=logging.INFO,
    debug=logging.DEBUG
)

level_to_str = {level: name
                for name, level in str_to_level.iteritems()}


class LogLevel(FieldProcessor):
    has_mutable_value = False
    def check_py(self, value):
        assert value in level_to_str.viewkeys()

    def check_raw(self, value):
        assert isinstance(value, StringTypes)
        assert value.lower() in str_to_level.viewkeys()

    def raw_to_py(self, raw_value):
        return str_to_level[raw_value.lower()]

    def py_to_raw(self, py_value):
        return level_to_str[py_value]


class LogHandler(FieldProcessor):
    pass
