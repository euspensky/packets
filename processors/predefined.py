# -*- coding:utf-8 -*-
from . import Number, String, Variant, Object, StrDate, StrUnixtime, LogLevel, Bytes, KeyNumber, TimeZone
from .numeric import unixtime


float_t = Number(float)
double_t = float_t

int8_t = Number(int, -128, 127)
uint8_t = Number(int, 0, 255)

int16_t = Number(int, -32768, 32767)
uint16_t = Number(int, 0, 65535)

int32_t = Number(int, -2147483648, 2147483647)
uint32_t = Number(int, 0, 4294967295)
int_t = int32_t
uint_t = int32_t

int64_t = Number(long, -9223372036854775808, 9223372036854775807)
uint64_t = Number(long, 0, 18446744073709551615)

long_t = int64_t
unixtime_t = Number(unixtime, -9223372036854775808, 9223372036854775807)

bool_t = Number(bool)

string_t = String()

any_t = Variant()

object_t = Object()

str_datetime_z_t = StrDate(format_string='%Y-%m-%d %H:%M:%S %z')
str_datetime_t = StrDate(format_string='%Y-%m-%d %H:%M:%S')
str_date_t = StrDate(format_string='%Y-%m-%d')
str_unixtime_t = StrUnixtime(format_string='%Y-%m-%d %H:%M:%S')

log_level_t = LogLevel()
bytes_t = Bytes()

str_int_t = KeyNumber(int)

tz_t = TimeZone()