# -*- coding:utf-8 -*-
from types import StringTypes

from . import FieldProcessor


class Bytes(FieldProcessor):
    has_mutable_value = False

    def __init__(self, strip=None):
        self._strip = strip

    def check_py(self, value):
        assert isinstance(value, StringTypes), (value, type(value))

    check_raw = check_py

    def raw_to_py(self, raw_value):
        if isinstance(raw_value, unicode):
            value = raw_value.encode('utf-8')
        else:
            value = raw_value
        if self._strip is not None:
            value = value.strip(self._strip)
        return value

    def py_to_raw(self, py_value):
        return py_value
