# -*- coding:utf-8 -*-
from itertools import izip

from ..base import PacketBase
from . import FieldProcessor, SubPacket


class Tuple(FieldProcessor):
    """Кортеж из разнотипных элементов"""
    def __init__(self, *element_types):
        self._element_types = []
        for element_type in element_types:
            if isinstance(element_type, FieldProcessor):
                self._element_types.append(element_type)
            elif issubclass(element_type, PacketBase):
                self._element_types.append(SubPacket(element_type))
            else:
                raise TypeError('element_type должен быть FieldType или Packet, получено %s', type(element_type))
        self._length = len(self._element_types)

    def check_py(self, py_value):
        assert isinstance(py_value, (list, tuple)), (py_value, type(py_value))
        if len(py_value) != self._length:
            raise ValueError('Некорректная длина списка - %s (нужно %s)' % (len(py_value), self._length))
        for t, v in izip(self._element_types, py_value):
            t.check_py(v)

    def check_raw(self, raw_value):
        assert isinstance(raw_value, (list, tuple)), (raw_value, type(raw_value))
        if len(raw_value) != self._length:
            raise ValueError('Некорректная длина списка - %s (нужно %s)' % (len(raw_value), self._length))

    def raw_to_py(self, raw_sequence):
        return tuple(t.raw_to_py(v) for v, t in izip(raw_sequence, self._element_types))

    def py_to_raw(self, sequence):
        return tuple(t.py_to_raw(v) for v, t in izip(sequence, self._element_types))

