# -*- coding:utf-8 -*-
from . import FieldProcessor, SubPacket
from packets.base import PacketBase


class Set(FieldProcessor):
    """Множество однотипных элементов"""
    def __init__(self, element_type):
        super(Set, self).__init__()
        if isinstance(element_type, FieldProcessor):
            self._element_type = element_type
        elif issubclass(element_type, PacketBase):
            self._element_type = SubPacket(element_type)
        else:
            raise TypeError('element_type должен быть FieldType или Packet, получено %s' % type(element_type))

    def check_py(self, value):
        assert isinstance(value, (list, tuple, set)), (value, type(value))
        assert len(set(value)) == len(value), 'повторяющиеся элементы в %s' % value

    def check_raw(self, value):
        assert isinstance(value, (list, tuple)), (value, type(value))

    def raw_to_py(self, raw_sequence):
        return {self._element_type.raw_to_py(value) for value in raw_sequence}

    def py_to_raw(self, sequence):
        return [self._element_type.py_to_raw(value) for value in sequence]
