# -*- coding:utf-8 -*-
from collections import Sequence
from numbers import Integral

from packets.base import PacketBase
from packets.context import debug_context
from types import NoneType
from . import FieldProcessor, SubPacket


class Array(FieldProcessor):
    """Массив однотипных элементов"""
    @property
    def element_type(self):
        return self._element_type

    @property
    def length(self):
        return self._length

    def __init__(self, element_type, length=None):
        super(Array, self).__init__()
        assert isinstance(length, (NoneType, Integral)), (length, type(length))
        if isinstance(element_type, FieldProcessor):
            self._element_type = element_type
        elif issubclass(element_type, PacketBase):
            self._element_type = SubPacket(element_type)
        else:
            raise TypeError('element_type должен быть FieldType или Packet, получено %s' % type(element_type))
        self._length = length

    def check_py(self, value):
        assert isinstance(value, Sequence), (value, type(value))
        if self._length is not None and len(value) != self._length:
            raise ValueError('Некорректная длина списка - %s (нужно %s)' % (len(value), self._length))
        if __debug__:
            for i, element in enumerate(value):
                with debug_context(i, '[%s]'):
                    self._element_type.check_py(element)

    def check_raw(self, value):
        assert isinstance(value, (list, tuple)), (value, type(value))
        if self._length is not None and len(value) != self._length:
            raise ValueError('Некорректная длина списка - %s (нужно %s)' % (len(value), self._length))

    def raw_to_py(self, raw_sequence):
        if __debug__:
            result = []
            for i, value in enumerate(raw_sequence):
                with debug_context(i, '[%s]'):
                    result.append(self._element_type.raw_to_py(value))
            return result
        else:
            return [self._element_type.raw_to_py(value) for value in raw_sequence]

    def py_to_raw(self, sequence):
        return [self._element_type.py_to_raw(value) for value in sequence]

