# -*- coding:utf-8 -*-
from . import FieldProcessor


class Variant(FieldProcessor):
    def check_py(self, py_value):
        pass

    def check_raw(self, raw_value):
        pass

    def raw_to_py(self, raw_value):
        return raw_value

    def py_to_raw(self, value):
        return value
