# -*- coding:utf-8 -*-
from numbers import Integral
import datetime
from types import StringTypes
import time

from .numeric import unixtime
from . import FieldProcessor


class StrDate(FieldProcessor):
    has_mutable_value = False

    def __init__(self, format_string):
        if format_string.endswith('%z'):
            self.raw_to_py = self.tz_raw_to_py
            self.py_to_raw = self.tz_py_to_raw
        self.format_string = format_string

    def check_py(self, value):
        assert isinstance(value, datetime.datetime), (value, type(value))

    def check_raw(self, value):
        assert isinstance(value, StringTypes), (value, type(value))

    def raw_to_py(self, raw_value):
        return datetime.datetime.strptime(raw_value, self.format_string)

    def py_to_raw(self, py_value):
        return py_value.strftime(self.format_string)

    def tz_raw_to_py(self, raw_value):
        naive_date_str, _, offset_str = raw_value.rpartition(' ')
        naive_dt = datetime.datetime.strptime(naive_date_str, self.format_string[:-3])
        return naive_dt.replace(tzinfo=datetime.FixedOffset.from_str(offset_str))

    def tz_py_to_raw(self, py_value):
        if py_value.tzinfo is None:
            py_value = py_value.replace(tzinfo=datetime.FixedOffset(-time.timezone // 60))
        return py_value.strftime(self.format_string)


class StrUnixtime(FieldProcessor):
    def __init__(self, format_string):
        if '%z' in format_string:
            raise NotImplementedError('Таймзона не поддерживается')
        self.format_string = format_string

    def check_py(self, value):
        assert isinstance(value, Integral), (value, type(value))

    def check_raw(self, value):
        assert isinstance(value, StringTypes), (value, type(value))

    def raw_to_py(self, raw_value):
        return unixtime(time.mktime(datetime.datetime.strptime(raw_value, self.format_string).timetuple()))

    def py_to_raw(self, py_value):
        return datetime.datetime.fromtimestamp(py_value).strftime(self.format_string)
