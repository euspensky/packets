# -*- coding:utf-8 -*-
from collections import Mapping
from ..base import PacketBase
from . import FieldProcessor, SubPacket, Number
from packets.context import debug_context


class Hash(FieldProcessor):
    """Хэш с однотипными элементами"""
    def __init__(self, key_type, element_type):
        if isinstance(element_type, FieldProcessor):
            self._element_type = element_type
        elif issubclass(element_type, PacketBase):
            self._element_type = SubPacket(element_type)
        else:
            raise TypeError('element_type должен быть FieldType или Packet, получено %s', type(element_type))

        if isinstance(key_type, FieldProcessor):
            if isinstance(key_type, Number):
                self._key_type = key_type.to_key_number()
            else:
                self._key_type = key_type
        elif issubclass(key_type, PacketBase):
            self._key_type = SubPacket(key_type)
        else:
            raise TypeError('key_type должен быть FieldType или Packet, получено %s', type(element_type))

    def check_py(self, value):
        assert isinstance(value, Mapping), (value, type(value))
        if __debug__:
            for key, element in value.iteritems():
                with debug_context(key, '[%s]'):
                    self._key_type.check_py(key)
                    self._element_type.check_py(element)

    def check_raw(self, raw_value):
        assert isinstance(raw_value, Mapping), (raw_value, type(raw_value))

    def raw_to_py(self, mapping):
        if __debug__:
            result = {}
            for key, value in mapping.iteritems():
                with debug_context(key, '[%s]'):
                    result[self._key_type.raw_to_py(key)] = self._element_type.raw_to_py(value)
        else:
            result = {
                self._key_type.raw_to_py(key): self._element_type.raw_to_py(value)
                for key, value in mapping.iteritems()
            }
        return result

    def py_to_raw(self, mapping):
        return {
            self._key_type.py_to_raw(key): self._element_type.py_to_raw(value)
            for key, value in mapping.iteritems()
        }
