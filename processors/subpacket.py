# -*- coding:utf-8 -*-
from . import FieldProcessor
from ..base import PacketBase
from collections import Mapping


class SubPacket(FieldProcessor):
    @property
    def sub_packet_type(self):
        return self._sub_packet_type

    def __init__(self, sub_packet_type):
        assert issubclass(sub_packet_type, PacketBase)
        self._sub_packet_type = sub_packet_type

    def check_py(self, py_value):
        assert isinstance(py_value, self._sub_packet_type), (py_value, type(py_value), self._sub_packet_type)

    def check_raw(self, raw_value):
        pass

    def raw_to_py(self, raw_value):
        return self._sub_packet_type.load(raw_value)

    def py_to_raw(self, value):
        return value.dump()

    def __repr__(self):
        return '<%s: %s>' % (self.__class__.__name__, self._sub_packet_type)
