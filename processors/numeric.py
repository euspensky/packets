# -*- coding:utf-8 -*-
from numbers import Integral, Real
import datetime
from . import FieldProcessor


class unixtime(int):
    def __str__(self):
        return '<%s>' % datetime.datetime.fromtimestamp(self).strftime('%Y-%m-%d %H:%M:%S')


class Number(FieldProcessor):
    has_mutable_value = False

    valid_types = {
        unixtime: Integral,
        int: Integral,
        long: Integral,
        float: Real,
        bool: Integral,
    }

    def __init__(self, number_type, min_=None, max_=None):
        self._number_type = number_type
        self.min = min_
        self.max = max_
        self._type_for_checks = self.valid_types[number_type]

    def check_py(self, value):
        assert isinstance(value, self._type_for_checks), (value, type(value), self._type_for_checks)
        if self.min is not None and value < self.min:
            raise ValueError('Invalid number: %s < %s' % (value, self.min))
        if self.max is not None and value > self.max:
            raise ValueError('Invalid number: %s > %s' % (value, self.max))

    def check_raw(self, value):
        if self.min is not None and value < self.min:
            raise ValueError('Invalid number: %s < %s' % (value, self.min))
        if self.max is not None and value > self.max:
            raise ValueError('Invalid number: %s > %s' % (value, self.max))

    def raw_to_py(self, raw_value):
        processed_value = self._number_type(raw_value)
        return processed_value

    def py_to_raw(self, value):
        return self._number_type(value)

    def to_key_number(self):
        return KeyNumber(self._number_type, self.min, self.max)


class KeyNumber(Number):
    def check_py(self, value):
        value = self._number_type(float(value))
        super(KeyNumber, self).check_py(value)

    check_raw = check_py

    def raw_to_py(self, raw_value):
        processed_value = self._number_type(float(raw_value))
        return processed_value

    def py_to_raw(self, value):
        return str(value)
