# -*- coding:utf-8 -*-
from . import FieldProcessor
from collections import Mapping


class Object(FieldProcessor):
    def check_py(self, py_value):
        assert isinstance(py_value, Mapping)

    def check_raw(self, raw_value):
        assert isinstance(raw_value, Mapping)

    def raw_to_py(self, raw_value):
        return raw_value

    def py_to_raw(self, value):
        return value
