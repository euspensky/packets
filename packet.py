# -*- coding:utf-8 -*-
from collections import OrderedDict, Counter
from itertools import izip
from operator import itemgetter
from abc import ABCMeta
from .base import PacketBase, FieldBase


class PacketMetaBase(ABCMeta):
    @staticmethod
    def _update_namespace(bases, namespace, add_slots):
        fields = OrderedDict()
        tags = set(namespace.get('packet_tags', []))
        super_slots = set()
        slots = set(namespace.get('__slots__', []))

        for base in bases:
            if hasattr(base, '__fields__'):
                for field_name, field in base.__fields__.iteritems():
                    assert field_name not in fields, 'Повторяющееся поле %s (класс %s)' % (field_name, base)
                    fields[field_name] = field
                    slots.add(field_name)

            if hasattr(base, '__tags__'):
                tags.update(base.__tags__)
            if hasattr(base, '__slots__'):
                super_slots.update(base.__slots__)

        not_inherited_fields = {field_name: field for field_name, field in namespace.iteritems()
                                if isinstance(field, FieldBase)}
        for field_index, (field_name, field) in enumerate(sorted(not_inherited_fields.iteritems(), key=itemgetter(1))):
            field.on_packet_class_create(fields.get(field_name), field_name, field_index)
            fields[field_name] = field
            if field_name not in slots:
                slots.add(field_name)
            namespace.pop(field_name)

        for field_name, count in Counter(field.name for field in fields.itervalues()).iteritems():
            if count > 1:
                raise TypeError('В пакете два поля с одинаковым name: %s' % field_name)

        if add_slots:
            namespace['__slots__'] = list(slots - super_slots)

        namespace.setdefault('__fields__', OrderedDict()).update(fields)
        namespace['__tags__'] = tags
        namespace['__raw_mapping__'] = OrderedDict((field.name, field.py_name)
                                                   for field_name, field in fields.iteritems())
        namespace['__mutable_value_fields__'] = frozenset(field_name for field_name, field in fields.iteritems() if
                                                          field.has_mutable_value)
        namespace['__immutable_value_fields__'] = frozenset(field_name for field_name, field in fields.iteritems() if
                                                            not field.has_mutable_value)

    def __getattribute__(self, item):
        fields = super(PacketMetaBase, self).__getattribute__('__fields__')
        if item in fields:
            return fields[item]
        return super(PacketMetaBase, self).__getattribute__(item)


class PacketMetaNoSlots(PacketMetaBase):
    def __new__(mcs, cls_name, bases, namespace):
        """Объединяем все __fields__ родителей, и добавляем в него определенные в самом классе поля
        """
        mcs._update_namespace(bases, namespace, add_slots=False)
        return PacketMetaBase.__new__(mcs, cls_name, bases, namespace)


class PacketMeta(PacketMetaNoSlots):
    def __new__(mcs, cls_name, bases, namespace):
        """Объединяем все __fields__ родителей, и добавляем в него определенные в самом классе поля
        """
        mcs._update_namespace(bases, namespace, add_slots=True)
        return PacketMetaBase.__new__(mcs, cls_name, bases, namespace)


class Packet(PacketBase):
    __metaclass__ = PacketMeta

    @classmethod
    def _parse_raw(cls, raw_js):
        attrs = {}
        for field_name, field in cls.__fields__.iteritems():
            raw_value = raw_js.get(field.name, None)
            field_value = field.raw_to_py(raw_value)
            attrs[field_name] = field_value
        return attrs

    def dump(self):
        js_dict = {}
        for field_name, field in self.__fields__.iteritems():
            if not field.lazy:
                raw_value = field.py_to_raw(getattr(self, field_name))
            else:
                raw_value = field.extract_raw(self)
            if raw_value is not None:
                js_dict[field.name] = raw_value
        return js_dict

    @classmethod
    def dump_partial(cls, partial_data):
        fields = cls.__fields__
        return {
            fields[field_name].name: fields[field_name].dump_partial(value)
            for field_name, value in partial_data.iteritems()
            if field_name in fields
        }


class ArrayPacket(PacketBase):
    __metaclass__ = PacketMeta

    @classmethod
    def _parse_raw(cls, raw_list):
        attrs = {}
        if len(raw_list) != len(cls.__fields__):
            raise ValueError('Неправильная длина списка')

        for (field_name, field), raw_value in izip(cls.__fields__.iteritems(), raw_list):
            field_value = field.raw_to_py(raw_value)
            attrs[field_name] = field_value
        return attrs

    def dump(self):
        return tuple(field.py_to_raw(getattr(self, field_name)) for field_name, field in self.__fields__.iteritems())

    def __iter__(self):
        for field_name in self.__fields__:
            yield getattr(self, field_name)



