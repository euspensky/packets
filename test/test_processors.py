# -*- coding:utf-8 -*-
from datetime import tzinfo

from pytz import timezone

from packets.field import Field
from packets.packet import Packet
import enum
import unittest
from packets.processors import *


class ProcessorsTestCase(unittest.TestCase):
    def test_numeric(self):
        processor = Number(long, 0, 8)
        self.assertRaises(AssertionError, processor.check_py, 0.0)
        self.assertRaises(ValueError, processor.check_py, -1)
        self.assertRaises(ValueError, processor.check_py, 9)
        self.assertIsInstance(processor.py_to_raw(5), long)

    def test_hash(self):
        processor = Hash(int_t, string_t)
        self.assertEqual({1: '2', 3: '4'}, processor.raw_to_py({'1.0': '2', '3': '4'}))
        self.assertEqual({'1': '2', '3': '4'}, processor.py_to_raw({'1': '2', '3': '4'}))

    def test_array(self):
        processor = Array(int_t, 3)
        self.assertEqual([1, 2, 3], processor.raw_to_py([1, 2, 3]))

    def test_enum(self):
        class Colors(enum.Enum):
            red = 1
            green = 2
            blue = 3


        processor = Enumeration(Colors)

        self.assertIs(processor.raw_to_py(2), Colors.green)
        self.assertEqual(processor.py_to_raw(Colors.red), 1)

    def test_name_enum(self):
        class Colors(enum.Enum):
            red = 1
            green = 2
            blue = 3

        processor = NameEnumeration(Colors)

        self.assertIs(processor.raw_to_py('green'), Colors.green)
        self.assertEqual(processor.py_to_raw(Colors.red), 'red')

    def test_string(self):
        processor = String(max_length=5, trim=True)
        self.assertEquals(processor.raw_to_py('123456'), '12345')
        self.assertEquals(processor.py_to_raw('654321'), '65432')

    def test_subpacket(self):
        class Sub(Packet):
            f1 = Field(int_t)


        processor = SubPacket(Sub)

        self.assertEquals(processor.py_to_raw(Sub(f1=3)), {'f1': 3})
        self.assertEquals(processor.raw_to_py({'f1': 4}).f1, 4)

    def test_bitmask(self):
        class Caps(enum.Enum):
            a = 0
            b = 1
            c = 2
            d = 3
            e = 4
            z = 31


        processor = BitMask(Caps)
        self.assertEquals(processor.py_to_raw({Caps.a, Caps.b, Caps.e}), 2 ** 0 + 2 ** 1 + 2 ** 4)
        self.assertEquals(processor.raw_to_py(17), {Caps.a, Caps.e})
        self.assertEquals(processor.raw_to_py(17 + 256), {Caps.a, Caps.e})
        self.assertEquals(processor.raw_to_py(2 ** 4 + 2 ** 31), {Caps.z, Caps.e})

    def test_tz(self):
        processor = tz_t
        self.assertIsInstance(processor.raw_to_py('Europe/Moscow'), tzinfo)
        self.assertIsInstance(processor.py_to_raw(timezone('Europe/Moscow')), 'Europe/Moscow')
