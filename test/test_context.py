import unittest
from packets.context import debug_context
from packets import Packet, ArrayPacket, Hash, Number, Field, int_t, Array


class ContextTestCase(unittest.TestCase):
    def test_debug_context(self):
        try:
            with debug_context('b', '%s'):
                with debug_context('a', '.%s'):
                    with debug_context('2', '[%s]'):
                        with debug_context('key', '[%s]'):
                            raise ValueError('pew')
        except ValueError as e:
            self.assertIn('b.a[2][key]', str(e))
        else:
            self.fail('ValueError not raised')

    def test_complex_packet_error(self):
        int_3 = Number(int, 0, 3)


        class Inner(ArrayPacket):
            f = Field(int_3)
            g = Field(int_3)


        class Outer(Packet):
            h = Field(Array(Hash(int_t, Inner)))


        data = {'h': [{}, {10: [1, 2], 20: [3, 4]}]}

        try:
            Outer.load(data)
        except ValueError as e:
            self.assertIn("<Outer>.h[1][20]<Inner>.g", str(e))
        else:
            self.fail('ValueError not raised')
