# -*- coding:utf-8 -*-
from copy import deepcopy
from .base import PacketBase, FieldBase
from packets.context import debug_context
from .processors import FieldProcessor, SubPacket, Const


_unset = object()


class Field(FieldBase):
    def __init__(self, processor=_unset, name=_unset, default=_unset, required=_unset, override=_unset):
        """Поле пакета
        @param processor: Обработчик поля (его тип)
        @param name: Имя поля при сохранении в json. Для полей ArrayPacket игнорируется.
        @param default: Значение по умолчанию в формате json.
        @param required: Признак обязательности.
        """
        self.spec = {}
        if processor is not _unset:
            self.spec.update(processor=processor)
        if name is not _unset:
            self.spec.update(name=name)
        if default is not _unset:
            self.spec.update(default=default)
        if required is not _unset:
            self.spec.update(required=required)
        if override is not _unset:
            self.spec.update(override=override)

    def _prepare(self, processor, name, default, required, override):
        if isinstance(processor, FieldProcessor):
            self.processor = processor
        elif issubclass(processor, PacketBase):
            self.processor = SubPacket(processor)
        else:
            raise TypeError('неправильный processor: %s' % type(processor))

        self.default = default
        if default is None:
            self.py_default = None
        else:
            self.processor.check_raw(self.default)
            self.py_default = self.processor.raw_to_py(self.default)

        self.required = required
        self.override = override

        self.name = name
        self.check = self.processor.check_py

        if hasattr(self.processor, 'max'):
            self.max = self.processor.max

        if hasattr(self.processor, 'min'):
            self.min = self.processor.min

        self.has_mutable_value = self.processor.has_mutable_value

    def on_packet_class_create(self, parent_field, field_name, field_index):
        super(Field, self).on_packet_class_create(parent_field, field_name, field_index)
        if parent_field is not None:
            if not self.spec.get('override', False):
                raise TypeError('Повторяющееся поле %s' % field_name)
            else:
                spec = parent_field.spec.copy()
                spec.update(self.spec)
                self.spec = spec
        elif parent_field is None:
            if self.spec.get('override', False):
                raise TypeError('Поле %s с флагом override ничего не переопределяет' % field_name)

        self.spec.setdefault('name', field_name)
        self.spec.setdefault('default', None)
        self.spec.setdefault('required', False)
        self.spec.setdefault('override', False)
        self._prepare(**self.spec)

    def raw_to_py(self, raw_value):
        if raw_value is None:
            if self.py_default is None:
                py_value = None
            else:
                py_value = deepcopy(self.py_default)
        else:
            if __debug__:
                with debug_context(self.name):
                    self.processor.check_raw(raw_value)
                    py_value = self.processor.raw_to_py(raw_value)
            else:
                self.processor.check_raw(raw_value)
                py_value = self.processor.raw_to_py(raw_value)

        if self.required and py_value is None:
            raise ValueError('Отсутсвует обязательное поле %s' % self)
        return py_value

    def py_to_raw(self, py_value):
        if py_value is None:
            if self.py_default is None:
                raw_value = None
            else:
                raw_value = deepcopy(self.default)
        else:
            if __debug__:
                with debug_context(self.py_name):
                    self.processor.check_py(py_value)
                    raw_value = self.processor.py_to_raw(py_value)
            else:
                self.processor.check_py(py_value)
                raw_value = self.processor.py_to_raw(py_value)

        if self.required and raw_value is None:
            raise ValueError('Отсутсвует обязательное поле %s' % self)

        return raw_value

    def clone(self, **kwargs):
        params = self.spec.copy()
        params.update(kwargs)
        field = self.__class__(**params)
        return field
